@echo off
chcp 1251

if "%1"=="/?" (
cls
echo copying files enter source the enter destination directory and enter 1 or 2 or 3
echo enter number 1 for replace while copying
echo enter number 2 for add files without replace
echo enter number 3 for delete files if files doesnt  exist in source directory
pause
exit /b 128
)

cd %~dp0
cls
set /p from="enter source directory: "
cd %from%
)
cls
set to=
set /p to="enter destination directory: "
cd ..
if "%to%"=="" (
md Backup
set to=Backup
cd Backup
goto menu
)
cd %to%>nul
cls

:menu
cd ..\%from%
cls
echo enter number 1 for replace while copying
echo enter number 2 for add files without replace
echo enter number 3 for delete files if files doesnt  exist in source directory
set /p command="enter command: "
if "%command%"=="1" (
goto command1
)
if "%command%"=="2" (
goto command2
)
if "%command%"=="3" (
goto command3
)
echo incorrect command
goto end

:command1
robocopy %from% ..\%to% /IS /E 
cls
echo job done
goto end

:command2
cd %from%
for %%i in (*) do (
echo %%i
if not exist ..\%to%\%%i (
copy %%i ..\%to%\
)
)
echo job done
goto end

:command3
cd ..\%to%
for %%i in (*) do (
echo %from%\%%~nxi
if not exist %from%\%%~nxi (
del %%i
)
)
echo job done
exit /b 128

:end
pause