#include <iostream>
using namespace std;
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <stdio.h>
int main(int argc, char *argv[], char *envp[]){
char* catalog;
if(argc==1){
	cout<<"Директория не введена"<<endl;
	return 1;
} else {
	catalog=new char[strlen(argv[1])];
	strcpy(catalog,argv[1]);
}
int fd=open("/etc/passwd",O_RDONLY);
long u=lseek(fd,0,SEEK_END);
char* p=new char[u+1];
lseek(fd,0,SEEK_SET);
read (fd,p,u);
int k=0;
int len;
for(len=0,k=0;k<u-2;k++) if(p[k]=='\n')len++;
len++;
char** ui=new char*[len];
for(int i=0;i<len;i++) ui[i]=new char[20];
int* uid=new int[len];
k=0;
for(int i=0;i<len;i++){
	int j=0;
	for(j=0;j<20;j++){
		if(p[k]!=':') {
			ui[i][j]=p[k];
			k++;
		}
		else {
			k++;
			ui[i][j]='\0';
			break;
		}
	}
	k=k+2;
	char r[6];
	int w;
	for(w=0;p[k]!=':';w++,k++){
		r[w]=p[k];
		
	}
	r[w+1]='\0';
	sscanf(r, "%i", &uid[i]);
	if(i==len-1) break;
	while(p[k]!='\n') {
		k++;
	}
	k++;
}
DIR* c=opendir(catalog);
struct dirent *dt;
dt=readdir(c);

while(dt!=NULL){
char name2[256];
strcpy(name2,catalog);
strcat(name2,"/");
int i=0;
for(i=0;i<strlen(dt->d_name);i++) name2[i+strlen(catalog)+1]=dt->d_name[i];
name2[strlen(dt->d_name)+strlen(catalog)+1]='\0';
struct stat buf;
stat(name2,&buf);
if(S_ISDIR(buf.st_mode)) cout<<"d";
else cout<<"-";
int mas[9];
int d=buf.st_mode;
for(int i=8;i>=0;i--){
	if(d%2==0) mas[i]=0;
	else mas[i]=1;
	d=d/2;
}
for(int i=0;i<9;i++){
	if(mas[i]==0) {
		cout<<"-";
		continue;
	}
	if((i+1)%3==0) {
		cout<<"x";
		continue;
	}
	if((i+1)%3==2) {
		cout<<"w";
		continue;
	}
	if((i+1)%3==1) {
		cout<<"r";
		continue;
	}
}
if((buf.st_nlink/10)!=0) cout<<"   "<<buf.st_nlink<<"  ";//ссылки
else cout<<"   "<<buf.st_nlink<<"   ";
for(int i=0;i<len;i++) {
	if(uid[i]==buf.st_uid)	cout<<ui[i]<<"      ";
}
for(int i=0;i<len;i++) {
	if(uid[i]==buf.st_gid)	cout<<ui[i]<<"      ";
}
cout<<buf.st_size<<"      ";
char* mBuf;
struct tm mytm = *localtime(&buf.st_mtime);//время последней модификации
switch(mytm.tm_mon){
	case 1:{cout <<"   янв.  ";break;}
	case 2:{cout <<"   февр. ";break;}
	case 3:{cout <<"   мар.  ";break;}
	case 4:{cout <<"   апр.  ";break;}
	case 5:{cout <<"   мая   ";break;}
	case 6:{cout <<"   июня  ";break;}
	case 7:{cout <<"   июля  ";break;}
	case 8:{cout <<"   авг.  ";break;}
	case 9:{cout <<"   сент. ";break;}
	case 10:{cout <<"   окт.  ";break;}
	case 11:{cout <<"   нояб. ";break;}
	case 12:{cout <<"   дек.  ";break;}
}
cout<<"      "<<mytm.tm_mday<<"      ";
if((mytm.tm_hour/10)==0) cout<<"0"<<mytm.tm_hour<<":";
else cout<<mytm.tm_hour<<":";
if((mytm.tm_min/10)==0) cout<<"0"<<mytm.tm_min;
else cout<<mytm.tm_min;
cout<<"      "<<dt->d_name<<endl;//имя 
dt=readdir(c);
}
delete [] ui;
delete [] uid;
return 0;
}
