#include <stdio.h>
#include<iostream>
#include<stdlib.h>
#include<string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <dirent.h>
using namespace std;
int main() {
	int fd;
	int g=0;
	char pole[256];
	size_t length;
	int i=0,j=0 ;
	int k;
	int l1,l2;
	struct A{
		int year;
		char name[256];
		char famil[256];
		char otchestvo[256];
		int key;
		int kolvo;

	}
	*ptr, *tmpptr;
	fd = open("mapped.dat",O_RDWR|O_CREAT,0666);
	if( fd == -1) {
		printf("File open failed!\n");
		exit (1);
	}
	length = 1000*sizeof(struct A);
	ftruncate(fd,length);
	ptr =(struct A*) mmap(NULL,length,PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);
	close(fd);
	if ( ptr == MAP_FAILED ){
		printf("Mapping failed!\n");
		exit (2);
	}
	cout<<"1.Ввод данных с заданным значением ключа\n2.Вывод одинаковых записей\n3.Вывод всех записей со значением года рождения в заданном диапазоне"<<endl;
	cin >> i;
	if(i==1){
		cout<<"Введите ключ"<<endl;
		cin>>k;
		tmpptr=ptr;
		for(int i=0;i<10;i++){
			if(tmpptr->key==k){
				cout<<tmpptr->key<<"."<<tmpptr->name<<" "<<tmpptr->famil<<" "<<tmpptr->otchestvo<<" "<<tmpptr->year<<endl;
			}
			tmpptr++;
		}
	}
	else if(i==2){
		cout<<"1.Вывод имени\n2.Вывод фамилии\n3.Вывод отчества\n4.Вывод года"<<endl;
		cin>>j;
		tmpptr=ptr;
		if(j==1){
			cout<<"Введите имя"<<endl;
			cin>>pole;
			for(int i=0;i<1000;i++){
				if(!strcmp (tmpptr->name,pole)){
					cout<<tmpptr->key<<"."<<tmpptr->name<<" "<<tmpptr->famil<<" "<<tmpptr->otchestvo<<" "<<tmpptr->year<<endl;
				}
				tmpptr++;
			}
		}
		if(j==2){
			cout<<"Введите фамилию"<<endl;
			cin>>pole;
			for(int i=0;i<1000;i++){
				if(!strcmp (tmpptr->famil,pole)){
					cout<<tmpptr->key<<"."<<tmpptr->name<<" "<<tmpptr->famil<<" "<<tmpptr->otchestvo<<" "<<tmpptr->year<<endl;
				}
				tmpptr++;
			}
		}
		if(j==3){
			cout<<"Введите отчество"<<endl;
			cin>>pole;
			for(int i=0;i<1000;i++){
				if(!strcmp (tmpptr->otchestvo,pole)){
					cout<<tmpptr->key<<"."<<tmpptr->name<<" "<<tmpptr->famil<<" "<<tmpptr->otchestvo<<" "<<tmpptr->year<<endl;
				}
				tmpptr++;
			}
		}
		if(j==4){
				do
				{
					cout<<"Введите год рождения"<<endl;
					cin>>k;
				}while(k < 1900 || k > 2014);
				for(int i=0;i<1000;i++)
					{
						if(tmpptr->year==k)
						{
							cout<<tmpptr->key<<"."<<tmpptr->name<<" "<<tmpptr->famil<<" "<<tmpptr->otchestvo<<" "<<tmpptr->year<<endl;
						}
					}
					tmpptr++;
			}


	}
	else if(i==3){
		cout<<"Введите начальный год рождения"<<endl;
		cin >>l1;
		cout<<"Введите конечный год рождения"<<endl;
		cin >>l2;
		tmpptr = ptr;
		for(int l=0;l<10;l++){
			if(tmpptr->year>l1&&tmpptr->year<l2){
				cout<<tmpptr->key<<"."<<tmpptr->name<<" "<<tmpptr->famil<<" "<<tmpptr->otchestvo<<" "<<tmpptr->year<<endl;
			}
			tmpptr++;
		}
	}
	/* Прекращаем отображать файл в память,
	записываем
	содержимое отображения на диск и освобождаем память. */
	munmap((void *)ptr, length);
	return 0;
}
