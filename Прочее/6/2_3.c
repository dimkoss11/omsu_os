#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

int main() {
	int *array;
	int shmid; 
	int new = 1;
	int semid; 
	struct sembuf mybuf;
	char pathname[] = "storage.txt"; 

	key_t key; 

	if((key = ftok(pathname, 0) ) < 0) {
		printf("ERROR: cannot generate key\n");
		exit(-1);
	}	

	if((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t get semid\n");
		exit(-1);
	}
	
	mybuf.sem_op = -2;
	mybuf.sem_flg = 0;	
	mybuf.sem_num = 0;

	if(semop(semid, &mybuf, 1) < 0){
		printf("Can\'t wait for condition\n");
		exit (-1) ;
	}	
	
		
	
	if((shmid = shmget(key, 4 * sizeof(int) , 0666|IPC_CREAT|IPC_EXCL)) < 0) {
		if(errno != EEXIST) {
			printf("ERROR: cannot create shared memory\n");
			exit(-1);
		} 
		else {
			if((shmid = shmget(key, 4 * sizeof(int), 0)) < 0) {
				printf("ERROR: cannot find shared memory\n");
        		exit(-1);
			}
		}
		
		new = 0;
	}

	array = (int *)shmat(shmid, NULL, 0);

	if(array == (int *)(-1)){
		printf("ERROR: cannot attach shared memory\n");
		exit(-1);
	}
	int a;
	a = array[3] + 1;
	if(new) {
		array[0] = 0;
		array[1] = 0;
		array[2] = 1;
		array[3] = 1;
	} else {
		int i;
		for (i = 0; i < 1000000; i ++){
		    array[3] = a;
		}
		array[2] += 1;
		
	}
	
	printf("Третья программа. Первая программа была запущена %d раз, вторая -  %d  раз, третья - %d  раз. Всего программы были запущены  - %d \n",
 array[0], array[1], array[2], array[3]);
	
	printf("Condition is present program 3\n");
	
	if(shmdt(array) < 0) {
		printf("ERROR: cannot detach shared memory\n");
		exit(-1);
	}
 semctl(semid,0,0, 0);
shmctl( shmid, 0,NULL);
 	
	return 0;
}
