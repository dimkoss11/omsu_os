#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>
#include<stdio.h>
#include<sys/ipc.h>
#include<sys/sem.h>
#include<errno.h>

int main()
{
	key_t key_semafor = ftok("storage", 0);
	struct sembuf buf;
	int semid = semget(key_semafor, 1, 0666 | IPC_CREAT);
	int fd[2];
	char text[256];
	pipe(fd);
	pid_t whois = fork();//Функция pid_t fork() превращает один процесс в два идентичных, которые называют родитель и потомок.
	if(whois > 0)
	{
		
			buf.sem_op = 0;
			buf.sem_flg = 0;
			buf.sem_num = 0;
			semop(semid, &buf, 1);
			

			printf("Родитель передает текст ребенку: 1 \n");
			write(fd[1], "1", 256);

			buf.sem_op = 1;
			buf.sem_flg = 0;
			buf.sem_num = 0;
			semop(semid, &buf, 1);

			buf.sem_op = -2;
			buf.sem_flg = 0;
			buf.sem_num = 0;
			semop(semid, &buf, 1);

			read(fd[0], text, 256);
			printf("Родитель подучил : ");
			printf(text);
			printf("\n");	
			
	}	
	else
	{
		
			
			buf.sem_op = -1;
			buf.sem_flg = 0;
			buf.sem_num = 0;
			semop(semid, &buf, 1);

			read(fd[0], text, 256);
			printf("Ребенок получит : ");
			printf(text);
			printf("\n");
			

			write(fd[1], "2", 256);
			printf("Ребенок передал : ");
			printf("2");
			printf("\n");
		
			buf.sem_op = 2;
			buf.sem_flg = 0;
			buf.sem_num = 0;
			semop(semid, &buf, 1);
				
	}
	return 0;
}
