#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/sem.h>
#include<stdio.h>
#include<iostream>
#include <stdlib.h>


using namespace std;

int main()
{
	key_t key = ftok("Makefile", 0);
	if (key <0)
	{
	printf("Ошибка генерации ключа \n");
	exit(-1);
	}
	struct sembuf mybuf;
	int semid = semget(key, 1, 0666 | IPC_CREAT);
	if (semid <0)
	{
	printf("Нет доступа по ключу к массиву семафоров \n");
	exit(-1);
	}
	mybuf.sem_op = -5;
	mybuf.sem_flg = 0;
	mybuf.sem_num = 0;
	//semop(semid, &mybuf, 1);
	if (semop(semid, &mybuf, 1) <0 )
	{
	printf("Не получилось \n");
	exit(-1);
	}
	printf("Первая программа выполнила работу\n");
	return 0;
}
