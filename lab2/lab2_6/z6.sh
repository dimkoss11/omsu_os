set -x
cd "$( dirname "$0" )"
SCRIPTPATH=$(cd `dirname -- $0` && pwd)

if [ -z "$1" ]; then 
  :
elif
	[ $1 == "-help" ]; then
	echo 'Страница справки' 
	echo 'Введите исходную папку, папку назначения' 
	echo 'Если папка не задана копируется в папку по умолчанию' 
	echo 'режимы работы:' 
	echo '1 Перезапись файлов в папке назначение :' 
	echo '2 Добавление файлов в папку без перезаписи существующих:' 
	echo '3 Зеркалирование (если файла нет в исходной папке, то он удаляется в папке назнаечния):' 
	echo '4 Добавление новых файлов: в папке назнаечния сохраняется более новый по времени файл' 
fi

dst=""
src=""

echo 'Введите исходную папку :'
read src #
echo 'Введите папку назначения :'
read dst #
echo 'Введите команду :'
echo '1 Перезапись :'
echo '2 Добавление :'
echo '3 Зеркалирование :'
echo '4 Добавление новых файлов:'
read command #

if [ -z $dst ]; then
	if [ ! -d Backup ]; then
    	mkdir Backup
	fi
	dst="Backup"
fi

if [ -z $src ]; then
	src="src1"
fi

if [ -z $command ]; then 
  :
elif [ $command == 1 ]; then
	first
elif [ $command == "2" ]; then
	second
elif [ $command == "3" ]; then
	third
elif [ $command == "4" ]; then
	fourth
else echo 'Неправильная команда'
	read
fi

function first()
{
	cp -R -T "$src/" "$dst/"
	#cp -R -p "$src/" "$dst/"
	echo "Файлы скопированы"
}

function second()
{
	cp -R -n -T "$src/" "$dst/"
	#cp -R -n -p "$src/" "$dst/"
	echo "Файлы скопированы"
}

function third()
{
	#cd ../$dst/
	cd $dst
	for f in ./*
	do
	if [ ! -e ../$src/$f ]; then
	rm "$f"
	fi
	done
	echo "Файлы скопированы"
	cd ..
}

function fourth()
{
	cd $dst
	for f in ./*
	do
		if [ ../$src/$f -nt $f ]; then
			cp -f "../$src/$f" "."
		fi
	done
	cd ..
}

read
set +x
clear