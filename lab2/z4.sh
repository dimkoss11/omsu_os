cd "$( dirname "$0" )"
if [ -e $1 ]; then
  if [ -r $1 ]; then echo доступен на чтение
  else echo недоступен на чтение
  fi
  if [ -w $1 ]; then echo доступен на запись
  else echo недоступен на запись
  fi
  if [ -x $1 ]; then echo доступен на исполнение
  else echo недоступен на исполнение
  fi
else echo файл не найден
fi
read